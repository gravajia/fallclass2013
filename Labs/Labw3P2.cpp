#include <cstdlib>
#include <iomanip>
#include <iostream>
using namespace std;
int main(int argc, char** argv)
{
int x;
int y;
int z;

cout <<"Enter a value for X"<< endl;
cin >> x;
cout <<"Enter a value for y"<< endl;
cin >> y;
cout <<"x = "<< setw(3) << x << " y = " << setw(3) << y << endl;

cout << setw(80)<< setfill ('-')<< "" << endl;

z = x;
x = y;
y = z;

cout  << setfill(' ') <<"x = "<< setw(3)<< x <<" y = " << setw(3)<< y << endl;
return 0;
}